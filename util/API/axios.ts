import axios from "axios";

export const fetchAPI = axios.create({
    baseURL: "https://zuhot-cinema-api-tagname.onrender.com/api",
    headers: { 'zuhot-key': 'abc123456' }
});
export const fetchPaypalAPI = axios.create({
    baseURL: "https://api-m.sandbox.paypal.com"
});