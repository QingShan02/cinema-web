export const constants = {
    URL_IMAGES: "https://zuhot-cinema-images.s3.amazonaws.com/poster-movie/",
    ROOT_API: "https://zuhot-cinema-api-tagname.onrender.com/",
    ROOT_FE: "https://cinema-web-topaz.vercel.app/",
    URL_TOPPING: "https://zuhot-cinema-images.s3.amazonaws.com/topping/",
    URL_LOGO: "https://zuhot-cinema-images.s3.amazonaws.com/logo.png",
    USD: 23000
}